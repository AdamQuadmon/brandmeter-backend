import graphget from './fbgraphHelper';
import * as Sentry from '@sentry/node';



export default async function getMetric(insightId, access_token, config) {


    // instagram metric docs https://developers.facebook.com/docs/instagram-api/reference/user/insights/?locale=de_DE
    let now = new Date().valueOf() / 1000
    
    let data = {};
    for (let period of Object.keys(config)) {
        // period = Object.keys(metric)[0] //"day"
        data[period] = {};
        let scroll = config[period].scroll
        let metric = config[period].metric
        for (let t = scroll.start; t <= scroll.end; t += scroll.scroll) {
            // console.log("t",t,period);
            // for(let t = 1; t<=2*356; t+=30){ // scroll in 30day intervals over the last 2 years
            let since = (now - (24 * 3600) * t).toFixed(0);
            let until = (now - (24 * 3600) * (t - scroll.scroll)).toFixed(0);
            try {
                let option = (period === 'lifetime' ? {
                    metric,
                    period
                } : {
                    metric,
                    period,
                    since,
                    until
                });
                
                // set authentication
                option.access_token = access_token
                let res = await graphget(option, "/" + insightId + "/insights")
              
                
                for (let i in metric) { //parse the metrics into separate object
                    if (res.data && res.data[i] && res.data[i].values) {
                        //filter out null,0,"" (objects,numbers,strings)
                        let values = res.data[i].values.filter(o => {
                            switch (typeof o.value) {
                                case 'number':
                                    return o.value !== 0
                                case 'object':
                                    return Object.keys(o.value).length !== 0
                                case 'string':
                                    return o.value.length !== 0
                                default:
                                    return true
                            }
                        })
                        //aggregate multi dim value objects to one overall metric value
                        values = values.map(valobj => {
                            if (typeof valobj.value === 'object') {
                                valobj.value = Object.keys(valobj.value).reduce((akku, key) => valobj.value[key] + akku, 0)
                            }
                            return valobj
                        })
                        //add or create field for new metric value
                        data[period][metric[i]] = data[period][metric[i]] ? data[period][metric[i]].concat(values) : values
                    }
                }
            } catch (err) {
                console.error(err);
                Sentry.captureException(err);
            }
        }
    }
    // console.log(JSON.stringify(data,null,2));
    return data;
}
