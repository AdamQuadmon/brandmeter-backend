import moment from 'moment'
import oAuth2Client from './googleauthentication';
import * as Sentry from '@sentry/node';




export default async function getMetric(insightId, user, identity, config) {

    const authClient = await oAuth2Client(identity)


    for (let period of Object.keys(config)) {
        const endTime = moment().subtract(1, 'days').toISOString()
        const startTime = moment().subtract(config[period].scroll.end, 'days').toISOString()
        try {
            // request the metrics. Using the google oAuth client that automatically refreshes the 
            // access_token if needed
            let res = await authClient.request({
                url: 'https://mybusiness.googleapis.com/v4/accounts/' + user.page_account_id + '/locations:reportInsights',
                method: 'POST',
                data: {
                    "locationNames": [
                        insightId
                    ],
                    "basicRequest": {
                        "metricRequests": [{
                            "metric": "ALL",
                            "options": [config[period].timeRangeName]

                        }],
                        "timeRange": {
                            "startTime": startTime,
                            "endTime": endTime
                        }
                    }
                }
            })
            res = res.data.locationMetrics[0].metricValues

            
            let result = {[period]: {
            }}
            // filter for only the metrics we use
            res = res.filter(o => {return config[period].metric.includes(o.metric)})
            res.map(valObj => {
                // we change every metric layout to be similar with facebooks layout
                for(let i in valObj.dimensionalValues){

                    // googlemybusiness gives us sometimes empty metrics?? We just delete and skip
                    if(!valObj.dimensionalValues[i].value) {
                        delete valObj.dimensionalValues[i]
                        continue
                    }
                    valObj.dimensionalValues[i].end_time = valObj.dimensionalValues[i].timeDimension.timeRange.startTime
                    delete valObj.dimensionalValues[i].timeDimension
                }
                result[period][valObj.metric] = valObj.dimensionalValues
            })

            return result


        } catch (error) {
            console.error('Error fetching metrics from google', error)
            console.log(error.response.data)
            Sentry.captureException(error);
            return {statusCode: 500, error: error}

        }



    }




}