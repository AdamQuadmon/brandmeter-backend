import elasticBody from '../lib/elasticBody'
import client from '../lib/elasticConnect'
import getMetrics from '../lib/LinkedIn/linkedin'
import getMedia from '../lib/LinkedIn/linkedinMedia'
import * as Sentry from '@sentry/node';


export default async(page, user, identity, days, brand_id) => {

    // the regular API endpoint gives you just organic data. To get the statistics
    // from payed content, you have to ask the ads API endpoint
    try {
        const internal_type_mapping = {
            linkedin: {
                uniqueImpressionsCount: 'impressions_organic_unique',
                impressionCount: 'impressions_organic',
                likeCount: 'like_count',
                commentCount: 'comment_count',
                shareCount: 'share_count',
                clickCount: 'click_count',
                CompanyFollowedByMember: 'follower_count'

            }
        }

        const aggregation_type_mapping = {
            linkedin: {

                // impressions
                impressionCount: 'impressions',

                uniqueImpressionsCount: 'impressions_unique',


                // engagement
                likeCount: 'post_engagements',
                commentCount: 'post_engagements',
                shareCount: 'post_engagements',
                clickCount: 'post_engagements',

                // follower count
                CompanyFollowedByMember: 'follower_count'



            }
        }

        let linkedin_data = await getMetrics(page, identity.access_token, {
            day: {
                metric: Object.keys(internal_type_mapping.linkedin),
                scroll:{
                    start:1,
                    end:days,
                    scroll:30,
                }
            },
            lifetime: {
                metric: ['followerCountsByCountry']
            }
        })


        // get all media objects
        linkedin_data.media = await getMedia(page, identity.access_token)


        // create the elasticsearch body
        const elastic_body = await elasticBody(linkedin_data, aggregation_type_mapping, internal_type_mapping, 'linkedin', user.user_id, page, brand_id )

        // last step: push everything to elasticsearch

        const resp_fbi = await client.bulk({body:elastic_body})
    } catch (error) {
        console.error(error); 
        Sentry.captureException(error);
    }

    return { statusCode: 200 }



}