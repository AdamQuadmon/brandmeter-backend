import management from './lib/auth0management'
import * as Parallel from 'async-parallel';
import fetch from 'node-fetch'
import client from './lib/elasticConnect'
import assert from 'assert'

import * as Sentry from '@sentry/node';
Sentry.init({ dsn: process.env.SENTRY_DSN });


export const handler = async event => {

    try {
        // take all users that have the pages object in their metadata
        let users = await management.getUsers({
            search_engine: 'v3'
        })

        console.log('Received the following users from auth0', users)

        let results = {}

        var start = new Date();

        // start for every possible identity the ETL process
        await Parallel.each(users, async user => {

            const brandsResult = await client.search({
                index: 'brandmeter-user-data',
                body: {
                    query: {
                        bool: {
                            must: [ 
                                {
                                    match: {
                                        brand:true
                                    }
                                },
                                {
                                    match: {
                                        "auth0id.keyword": user.user_id
                                    }    
                                }
                            ]
                        }
                    }
                }
            }).catch(error => console.log( JSON.stringify(error, null,2)))
            assert.strictEqual(brandsResult.statusCode, 200)
            let value = brandsResult.body.hits.hits.map( result => {
                let x = result._source
                x.id = result._id
                return x
            })
            if(value) {
                await Parallel.each(value, async brand => {
                const pages = Object.values( brand.activePages).flat()

                await Parallel.each(pages, async page => {
                    const params = new URLSearchParams({
                        page: page.id,
                        type: page.type,
                        user_id: user.user_id,
                        brand_id: brand.id,
                        // the ID of the Facebook of GoogleMyBusiness Account that is owning this entity
                        page_account_id: page.user_id
                        // just load the last 10 days. If you omit this value, it takes max days
                        //days: 10
                    })

                    console.log('sending now the following', params)
                    // trigger the ETL function
                    try {
                        await fetch(process.env.ETL_URL + '?' + params)
                            .then(result => result.json())
                            .then(json => results[page.page_id] = json)
                        
                    } catch (error) {
                        console.error(error);
                        Sentry.captureException(error);
                    }

                    

                })

            } )}

        })

        console.log('done', new Date().getTime() - start.getTime());

        return {
            statusCode: 200,
            body: JSON.stringify({
                message: 'Triggerfunction run successful',
                results
            }, null, 2)
        }


    } catch (error) {
        console.log('Error triggering ETL function', error)
        Sentry.captureException(error)

    }





}