This is the backend worker for the brandmeter service. It is actually a serverless service that gets deployed to Amazon Lambda.  
The service needs to get triggered via HTTP in order to start working.   
It accepts one page and one user per call. It gets triggered for example from the frontend, when a new user signs-up
or via the `trigger-service` that is actually a cron-job.  
This service is using the auth_tokens that are stored in Auth0 and tries to load metrical data from the different pages
and push them to elasticsearch.  

To run this service, you need to have the framework `serverless` installed globally. Afterwards, you can spin up a local
testing environment with `serverless offline` 


To test the extract-transform-load job for instagram:
```
curl --location --request GET 'http://localhost:3000/v1/trigger/etl?user_id=facebook|10220646992273495&page=17841407111540019&type=instagram' --header 'Content-Type: application/json'
```

To test it for facebook: 
``` 
curl --location --request GET 'http://localhost:3000/v1/trigger/etl?user_id=facebook|10220646992273495&page=341725443279606&type=facebook' --header 'Content-Type: application/json'
```

## Image/video storage
We store static assets in our own S3 storage, the social media providers might decide to delete content after some time. Especially instagram stories are just very short-living.   
We use a minio s3 storage hosted in Kubernetes in order to store and deliver static files.

The software is currently able to catch metrics from 
+ Facebook Pages
+ Instagram Business Pages
+ Google Analytics Dataviews
+ Google Search Console Properties
+ Google MyBusiness
+ LinkedIn Business Pages
+ Saleor Store

The following table shows the mapping between metric names we get from the different APIs and our internal mapping that we use to store and aggregate statistics globally.
Values, that don't have a "Metric Name" are created internally and used for Aggregation only.

# Instagram
| Metric Name | Internal Name |Aggregated Type |Description EN|Description DE |
|---|---|---|---|---|
|follows_count| follower_count | | No insights metric, but GET /{ig-user-id}?fields=follows_count. Total number of unique accounts following this profile | Gesamtanzahl an Instagram followers |
||total_actions | total_actions |All website,contacts,directions,phone call clicks on a profile|Gesamtanzahl an Klicks auch Call-To-Actions Buttons|
|website_clicks|website_clicks|total_actions|||
|email_contacts|email_contacts|total_actions|||
|get_directions_clicks|get_directions_clicks |total_actions||
|text_message_clicks|text_message_clicks|total_actions||
|phone_call_clicks |phone_call_clicks|total_actions||
||engaged_users_unique|||
|impressions | impressions_unique| impressions_unique | Total number of unique accounts that have seen this profiles media |Gesamtanzahl der individuellen Aufrufe der Medienobjekte des Business-Kontos|

# Facebook
| Metric Name | Internal Name | Aggregated Type | Description EN| Description DE|
|---|---|---|---|---|
|page_fans | follower_count ||Lifetime: The total number of people who have liked your Page. (Unique Users) | Gesamtanzahl der Follower der Seite an diesem Tag |
|page_total_actions|total_actions||Daily: The number of clicks on your Page's contact info and call-to-action button|Die Anzahl der Klicks auf die Kontaktinformationen und den Call to Action-Button auf deiner Seite. |
|page_engaged_users|engaged_users_unique||Daily: The number of people who engaged with your Page. Engagement includes any click or story created. (Unique Users)||
|page_impressions_paid_unique|impressions_paid_unique ||Daily: The number of people who had any content from your Page or about your Page enter their screen through paid distribution such as an ad. (Unique Users)|Die Anzahl der Personen, die jeglichen Content von oder über deine Seite über bezahlte Distribution wie eine Werbeanzeige gesehen haben.|
|page_impressions_organic_unique| impressions_organic_unique || Daily: The number of people who had any content from your Page or about your Page enter their screen through unpaid distribution. This includes posts, stories, check-ins, social information from people who interact with your Page and more. (Unique Users) |Die Anzahl der Personen, die jeglichen Content von oder über deine Seite über eine unbezahlte Distribution gesehen haben. Dazu zählen u.a. Beiträge, Stories, Besuche und soziale Infos von Personen, die mit deiner Seite interagieren.|
|page_impressions_unique| impressions_unique |impressions_unique| Daily: The number of people who had any content from your Page or about your Page enter their screen through unpaid and paid distribution together. This includes posts, stories, check-ins, social information from people who interact with your Page and more. (Unique Users) |Die Anzahl der Personen, die jeglichen Content von oder über deine Seite über eine unbezahlte Distribution gesehen haben. Dazu zählen u.a. Beiträge, Stories, Besuche und soziale Infos von Personen, die mit deiner Seite interagieren.|
|page_cta_clicks_logged_in_total| cta_clicks | total_actions |Daily: the number of clicks on the Call-to-action button. If the buttons changed through the time, we just aggregate them |Die Gesamtanzahl an Klicks auf den "Call-To-Action" Button der Facebook Seite|
|page_call_phone_clicks_logged_in_unique |phone_call_clicks|total_actions|Daily: The total number of clicks on the phone call button| Die Gesamtanzahl an Klicks auf den "Jetzt anrufen" Button|
|page_get_directions_clicks_logged_in_unique |get_directions_clicks |total_actions |Daily: The total number of clicks on the "get direction" button | |
|page_website_clicks_logged_in_unique |website_clicks |total_actions ||

# Google MyBusiness
The Google MyBusiness API is currently not easy to get access to. Furthermore, it is among the worst documented APIs. Nevertheless, we got it running and take the following metrics:  

| Metric Name | Internal Name | Aggregated Type | Description EN| Description DE|
|---|---|---|---|---|
|QUERIES_DIRECT | queries_direct | | The number of times the resource was shown when searching for the location directly. | Die Anzahl an Impressionen wenn direkt nach der Location gesucht worden ist |
|QUERIES_INDIRECT | queries_indirect || The number of times the resource was shown as a result of a categorical search (for example, restaurant, bakery near me). | Die Anzahl an Impressionen wenn nach einer Kategorie gesucht worden ist (Z.B. Café in meiner Nähe, Friseur, etc.)|
| VIEWS_MAPS | views_maps | impressions_unique | The number of times the resource was viewed on Google Maps. | |
| VIEWS_SEARCH | views_search | impressions_unique | The number of times the resource was viewed on Google Search. ||
| ACTIONS_WEBSITE | website_clicks | total_actions | The number of times the website was clicked. ||
| ACTIONS_PHONE | phone_call_clicks |total_actions | The number of times the phone number was clicked. ||
| ACTIONS_DRIVING_DIRECTIONS | get_directions_clicks | total_actions | The number of times driving directions were requested. ||
